package com.sandman.upgenics_test.customViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.View;

import java.util.ArrayList;

public class BubblesCanvas extends View {


    private int bubblesPlacedOnScreen = 0;
    private int bubblesinRow = 0;
    private int bubblesinCol = 0;
    private int exactBubbleSize = 0;
    private boolean defualtDrawen = false;
    private Path bubblesCanvasPath = new Path();
    private Path poopedBubblesCanvasPath = new Path();
    private ArrayList<RectF> bubblesTracker = new ArrayList<>();




    public BubblesCanvas(Context context, int _bubblesPlacedOnScreen, int _bubblesinRow,int _bubblesinCol, int _exactBubbleSize) {
        super(context);
        bubblesPlacedOnScreen = _bubblesPlacedOnScreen;
        bubblesinRow = _bubblesinRow;
        bubblesinCol = _bubblesinCol;
        exactBubbleSize = _exactBubbleSize;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(!defualtDrawen) {
            canvas.drawColor(Color.WHITE);
            drawDefualtViews(canvas);
            defualtDrawen = true;
            Paint paint = new Paint();
            paint.setColor(Color.BLUE);
        }else{
            canvas.drawColor(Color.WHITE);

            Paint paint = new Paint();
            paint.setColor(Color.BLUE);
            canvas.drawPath(this.bubblesCanvasPath,paint);

            paint.setColor(Color.WHITE);
            canvas.drawPath(poopedBubblesCanvasPath,paint);


        }
    }
    private void drawDefualtViews(Canvas canvas){
        int middle = this. bubblesinCol / 2;
        for(int i= middle;i>0;i--){
            int updateerIndex = i * exactBubbleSize;
            this. drawCircle(canvas,exactBubbleSize/2,updateerIndex,exactBubbleSize/2 );
        }

        for (int i=1;i< this.bubblesinRow ; i++){
            int updateerIndex = i * exactBubbleSize;
            this. drawCircle(canvas,updateerIndex,exactBubbleSize/2,exactBubbleSize/2 );
        }
        for (int i=1;i< this.bubblesinCol ; i++){
            int x = (this. bubblesinRow * exactBubbleSize) - exactBubbleSize/2;
            int updateerIndex = i * exactBubbleSize;
            this. drawCircle(canvas,x,updateerIndex,exactBubbleSize/2 );
        }
        for (int i=bubblesinRow - 1; i > 0 ; i--){
            int y = (this. bubblesinCol * exactBubbleSize) - exactBubbleSize/2;
            int updateerIndex = i * exactBubbleSize;
            this. drawCircle(canvas,updateerIndex,y,exactBubbleSize/2 );
        }

        for(int i= this. bubblesinCol - 1; i>middle ;i--){
            int updateerIndex = i * exactBubbleSize;
            this. drawCircle(canvas,exactBubbleSize/2,updateerIndex,exactBubbleSize/2 );
        }

        for(int i= 1; i < this. bubblesinCol ;i++){
            int updateerIndex = i * exactBubbleSize;
            this. drawCircle(canvas,updateerIndex/2,updateerIndex,exactBubbleSize/2 );
        }

        for(int i= 1; i < this. bubblesinCol ;i++){
            int updateerIndex = i * exactBubbleSize;
            int rowsMax = exactBubbleSize * this. bubblesinRow;
            this. drawCircle(canvas,(rowsMax - updateerIndex/2),updateerIndex,exactBubbleSize/2 );
        }
    }

    private void drawCircle(Canvas canvas,int x, int y,int radius){
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        RectF circle = new RectF(x-radius,y-radius,x+(radius),y+(radius));
        canvas.drawRoundRect(circle,radius*2,radius*2,paint);
        this. bubblesCanvasPath.addRoundRect(circle, radius*2,radius*2,Path.Direction.CW);
        bubblesTracker.add(circle);
    }


    public void checkDragListneronCanvas(int x,int y){
        int radius = exactBubbleSize / 2;
        for(int i=0;i<bubblesTracker.size();i++){
            if(bubblesTracker.get(i).contains(x,y)){
                RectF rectF = bubblesTracker.get(i);
                poopedBubblesCanvasPath.addRoundRect(rectF,radius*2,radius*2,Path.Direction.CW);
//                poopedBubblesCanvasPath.addCircle(rectF.left+radius, rectF.top+radius, radius, Path.Direction.CW);
                bubblesTracker.remove(rectF);
                invalidate();
            }
        }

    }


}
