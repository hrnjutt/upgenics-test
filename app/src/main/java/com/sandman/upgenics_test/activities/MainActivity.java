package com.sandman.upgenics_test.activities;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;

import com.sandman.upgenics_test.customViews.BubblesCanvas;

public class MainActivity extends AppCompatActivity {

    private int bubblesPlacedOnScreen = 0;
    private int bubblesinRow = 0;
    private int bubblesinCol = 0;
    private int exactBubbleSize = 0;
    private BubblesCanvas bubblesCanvas = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this. calculateMaxBubbles();
        this. bubblesCanvas = new BubblesCanvas(this,bubblesPlacedOnScreen,bubblesinRow,bubblesinCol,exactBubbleSize);
        setContentView(bubblesCanvas);

    }

    private void calculateMaxBubbles(){
        try{
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int widthInPx = this. normalizeScreenWidthHeight(size.x);
            int heightInPx = this. normalizeScreenWidthHeight(size.y - titleBarHeight());
            this. exactBubbleSize = this. excatSizeOfBubleFitInView(widthInPx,heightInPx) * 3;
            this. bubblesinRow = (int) (widthInPx / exactBubbleSize);
            this. bubblesinCol = (int) (heightInPx / exactBubbleSize);

            this. bubblesPlacedOnScreen = bubblesinRow * bubblesinCol;

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private int excatSizeOfBubleFitInView(int screenWidth, int screenHeight){
        int circleDiameter = 0;
        for(int i=screenWidth/4;i>0;i--) {
            if(screenWidth % i ==0 && screenHeight % i ==0){
                circleDiameter = i;
                break;
            }
        }
        return circleDiameter;
    }

    private int titleBarHeight(){
        int statusBarHeight = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    private int normalizeScreenWidthHeight(int value){
        int _units = value / 10;
        value = _units * 10;
        return value;
    }


    int x_old, y_old = 0;
    int i = 1;
    private static final String TAG = "Touch";


    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        switch (event.getAction() & MotionEvent.ACTION_MASK) {
//           0 case MotionEvent.ACTION_DOWN:
//                onDraw((int)event.getX(),(int)event.getY());
//                Log.i(TAG, "coordinate x1 : "+String.valueOf(event.getX())+" y1 : "+String.valueOf(event.getY()));
//                break;
//            case MotionEvent.ACTION_MOVE:
                onDraw((int)event.getX(),(int)event.getY());
                Log.i(TAG, "coordinate x1 : "+String.valueOf(event.getX())+" y1 : "+String.valueOf(event.getY()));
//                break;
//        }
        return true;
    }


    public void onDraw(int x_click,int y_click){
        if(this. x_old == x_click && this. y_old == y_click){
            return;
        }else{
            this. x_old = x_click;
            this. y_old = y_click;
            this. bubblesCanvas.checkDragListneronCanvas(this. x_old, this. y_old);
        }
    }
}
